#!/bin/bash

sudo apt-get update && sudo apt-get upgrade -y

#================#
# Apache and PHP #
#================#
sudo apt-get install apache2 -y
sudo sh -c "echo 'ServerName localhost' >> /etc/apache2/apache2.conf"
sudo apt-get install -y php7.2{,-pgsql,-fpm} libapache2-mod-php7.2 -y
sudo service apache2 restart

# ========== #
# PostgreSQL #
# ========== #

# Creates a user with this username and password
APP_DB_USER=myuser
APP_DB_PASS=dbpass
APP_DB_NAME=mydb

# What postqresql version to install
PG_VERSION=10

sudo apt-get install postgresql{-"$PG_VERSION",-contrib} -y

PG_CONF="/etc/postgresql/$PG_VERSION/main/postgresql.conf"
PG_HBA="/etc/postgresql/$PG_VERSION/main/pg_hba.conf"
PG_DIR="/var/lib/postgresql/$PG_VERSION/main"

# Listen to all addresses
sed -i "s/#listen_addresses = 'localhost'/listen_addresses = '*'/" "$PG_CONF"
# Append to pg_hba.conf to add password auth:
echo "host    all             all             all                     md5" >> "$PG_HBA"
# Set default encoding
echo "client_encoding = utf8" >> "$PG_CONF"

sudo service postgresql restart

# Create a simple database
cat << EOF | sudo -i -u postgres psql
-- Create the database user:
CREATE USER $APP_DB_USER WITH PASSWORD '$APP_DB_PASS' CREATEDB;
-- Create the database:
CREATE DATABASE $APP_DB_NAME WITH OWNER=$APP_DB_USER
                                  LC_COLLATE='en_US.utf8'
                                  LC_CTYPE='en_US.utf8'
                                  ENCODING='UTF8'
                                  TEMPLATE=template0;
EOF
