# Skapa en vagrant miljö med Apache 2 och PHP 7.2 och PostgreSQL 10

## TL;DR

Kort version av att starta upp vagrantmiljön.

```
$ git clone git@bitbucket.org:pabo1800/vagrant-lapp.git my-project
$ cd my-project
$ git clone git@bitbucket.org:miun_dsv_misc_courses/pabo1800-dt161g.git www
$ (umask 000; mkdir www/writeable)
$ vagrant up
```

- Nu bör du kunna se Apaches infosida i din webbläsare på
  [loalhost:8080](localhost:8080). Projektsidan ligger på
  [localhost:8080/dt161g](localhost:8080/dt161g).

Lägg till servern i pgadmin:

- server: `localhost`
- port: `15432`
- username: `myuser`
- password: `dbpass`

Användaren myuser har rättigheter att skapa nya databaser, vilket vi inte har på
miun.

## Förberedelse

- Installera virtualbox, vagrant och pgadmin.
- Skapa en projektmapp någonstans och packa upp zip filen i mappen.

  Alternativt, klona git repot

  ```
  $ git clone git@bitbucket.org:pabo1800/vagrant-lapp.git my-project
  ```

Clona ditt eget repo och döp om mappen till `www`. **Den mappen måste finnas för
att vagrantmiljön ska skapas**. Så se till att göra det **innan** ni kör
vagrant kommandot.

```sh
$ git clone git@bitbucket.org:miun_dsv_misc_courses/pabo1800-dt161g.git www
```

```
my-project
├── bionic-apache-php.zip
├── README.md
├── setup.sh
├── Vagrantfile
└── www
    ├── dt161g
    │   ├── index.php
    │   ├── lab1
    │   ├── lab2
    │   ├── lab3
    │   ├── lab4
    │   ├── phpinfo.php
    │   ├── project
    │   └── test.php
    └── README.md
```

## Möjlig konfigurering

Filen `setup.sh` innehåller följande variabler som används för att skapa en
användare och en databas i postgresql. Om du vill kan du ändra dessa.

```
APP_DB_USER=myuser
APP_DB_PASS=dbpass
APP_DB_NAME=mydb
```

## Skapa vagrantmiljön

Skapa och starta vagrant miljön. Första gången kommer vagrant att ladda ner
en Ubuntu bionic64 "box", i princip en färdig virtuell maskin. Sedan startas
maskinen och skriptet `setup.sh` körs. Scriptet installerar Apache med PHP samt
PostgreSQL och skapar en användare och en databas.

Användaren har rättigheter att skapa nya databaser, vilket inte gäller för våra
användare på miuns server. Men gör det lätt att testa saker i en databas och
sedan kasta den.

```sh
$ vagrant up
```

Processen tar ett par minuter på min dator. När det är klart kan du se miljön
som en maskin i Virtualbox. Men vi behöver inte hantera virtualbox alls när vi
utan vagrant sköter det åt oss.

- Nu bör du kunna se Apaches infosida i din webbläsare på
  [loalhost:8080](localhost:8080). Projektsidan ligger på
  [localhost:8080/dt161g](localhost:8080/dt161g).

## Filen `writeable`

- För att testfilen
  ([test.php](localhost:8080/dt161g/test.php))
  ska fungera måste man skapa mappen `writeable` i `www` mappen med fulla
  rättigheter.

```
$ (umask 000; mkdir www/writeable)  # () gör att umask bara gäller tillfälligt
```

Nu ser filstrukturen ut så här

```
my-project
├── bionic-apache-php.zip
├── README.md
├── setup.sh
├── Vagrantfile
└── www
    ├── dt161g
    │   ├── index.php
    │   ├── lab1
    │   ├── lab2
    │   ├── lab3
    │   ├── lab4
    │   ├── phpinfo.php
    │   ├── project
    │   └── test.php
    ├── README.md
    └── writeable
```

## Lägg till databasservern i pgadmin

- Starta pgpadmin och clicka på `Object -> Create -> Server...`.
- Ange ett namn på servern, vilket som helst
- Klicka på Connecion tabben och ange `localhost` som hostname, `15432` som
  port och det användarnamn och lösenord som står i `setup.sh` . Om du inte
  ändrat något är det `myuser` och `dbpass`.
- Nu ska servern ligga i listan i "Servers" (bredvid Miuns server om du har
  laggt till den).

## Hantera vagrantmiljön

- Stäng ner vagrant miljön med

```
$ vagrant halt
```

- Starta miljön igen med

```
$ vagrant up
```

- Om du vill ta bort miljön så måste du stänga av den först

```
$ vagrant halt
$ vagrant destroy default
```

## Arbetsflöde

- Arbeta som vanligt med innehållet i `www` i din editor, och använd
  webbläsaren för att se resultatet på
  [localhost:8080/dt161g](localhost:8080/dt161g).

- Gör commits och pusha till bitbucket som vanligt.

- När du ska redovisa måste du skicka upp innehållet i `www` mappen till Miuns
  FTP-server med ex FileZilla så att du kan se innehållet på
  `studenter.miun.se/~portalid/dt161g`.
